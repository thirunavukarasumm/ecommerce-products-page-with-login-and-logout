function fetchData(API) {
    return fetch(API)
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw `${data.statusText} ${data.status}`;
            }
        });
}

function groupDataBasedOnCategory(items) {
    return items.reduce((acc, item) => {
        if (acc[item.category]) {
            acc[item.category].push(item);
        } else {
            acc[item.category] = [item];
        }
        return acc;
    }, {});
}

function createAElement(tag, ClassName, idName, content) {
    let div = document.createElement(tag);
    div.classList.add(ClassName);
    if (content) {
        div.innerHTML = content;
    }
    if (idName) {
        div.setAttribute("id", idName)
    }
    return div;
}

function getSingleProduct(API) {
    fetchData(API)
        .then((item) => {
            let sectionOne = document.getElementsByClassName("section-one-part-one");
            let sectionTwo = document.getElementsByClassName("section-one-part-two");
            sectionOne[0].innerHTML = `<img src=${item.image} alt="" srcset="">`
            let cardContent = `
                <div class="card">
                    <h3>${item.title}</h3>
                    <h4>$${item.price}</h4>
                    <p> ${item.description}</p>
                    <h4> Rating - ${item.rating["rate"]} | Reviews - ${item.rating["count"]} </h4>
                    <button> Add to cart </button>
                </div>
            `
            
            sectionTwo[0].innerHTML = cardContent;
            
        })
        
        .catch((err) => {
            let section = document.getElementsByClassName("section-one-part-two");
            let errorContent = "";
            if (typeof err === "string") {
                errorContent = `<div class="title">
                                        <h1>${err}</h1>
                                    </div>`
            } else {
                errorContent = `<div class="title">
                                        <h1>Error while fetching the API.</h1>
                                    </div>`
            }
            let wrapDiv = createAElement("div", "wrap", null, errorContent);
            section[0].appendChild(wrapDiv)

            console.log(err);
        })


}


window.onload = function () {
    let url = document.location.href,
        params = url.split('?')[1].split('&')[0].split("=")[1]

    getSingleProduct(`https://fakestoreapi.com/products/${params}`)

    
}



function logout() {
    sessionStorage.removeItem("isLoggedIn");
    alert("Logged Out successfully.");
    window.location.reload();
}

function loggedIn() {
    let login = document.getElementsByClassName("login")[0]
    login.style.display = "none"
    let signup = document.getElementsByClassName("signup")[0]
    signup.style.display = "none"
    let card = document.getElementsByClassName("card");
    card[0].innerHTML = "";
    let element = document.createElement("p");
    element.innerHTML = "You are currently logged in. Click logout to perform other operations."
    card[0].appendChild(element)

}

if (sessionStorage.getItem("isLoggedIn")) {
    loggedIn()
} else {
    let logout = document.getElementsByClassName("logout")[0]
    logout.style.display = "none"
}