function fetchData(API) {
    return fetch(API)
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw `${data.statusText} ${data.status}`;
            }
        });
}

function groupDataBasedOnCategory(items) {
    return items.reduce((acc, item) => {
        if (acc[item.category]) {
            acc[item.category].push(item);
        } else {
            acc[item.category] = [item];
        }
        return acc;
    }, {});
}

function createAElement(tag, ClassName, idName, content) {
    let div = document.createElement(tag);
    div.classList.add(ClassName);
    if (content) {
        div.innerHTML = content;
    }
    if (idName) {
        div.setAttribute("id", idName)
    }
    return div;
}

function getAllProducts(API) {
    fetchData(API)
        .then((items) => {
            if(items.length === 0){
                throw "No products available";
            } else {
                return groupDataBasedOnCategory(items);

            }
        })
        .then((groupedObjects) => {
            let section = document.getElementsByClassName("section-one-part-two");
            section[0].innerHTML = ""
            for (let key in groupedObjects) {
                let titleContent = `<div class="title">
                                        <h1>${key}</h1>
                                    </div>`
                let wrapDiv = createAElement("div", "wrap", "wrap", titleContent);
                let content = createAElement("div", "contents")
                groupedObjects[key].forEach((item) => {
                    let cardContent = `
                        <img src=${item.image} alt="" srcset="">
                        <div class="content-spacing">
                            <h3>${item.title}</h3>
                            <h4>$${item.price}</h4>
                            <p> ${item.description}</p>
                            <h4> Rating - ${item.rating["rate"]} | Reviews - ${item.rating["count"]} </h4>
                            <button> Add to cart </button>
                        </div>
                    `
                    let card = createAElement("div", "card", null, cardContent);
                    let a = createAElement("a")
                    a.setAttribute("href", `https://ecommerce-products-page-with-login-and-logout.vercel.app/productPage.html?productId=${item.id}`)
                    a.appendChild(card)
                    content.appendChild(a)
                })
                wrapDiv.appendChild(content)
                section[0].appendChild(wrapDiv)
            }
            

        })
        .catch((err) => {
            let section = document.getElementsByClassName("section-one-part-two");
            let errorContent = "";
            if (typeof err === "string") {
                errorContent = `<div class="title">
                                        <h1>${err}</h1>
                                    </div>`
            } else {
                errorContent = `<div class="title">
                                        <h1>Error while fetching the products.</h1>
                                    </div>`
            }
            let wrapDiv = createAElement("div", "wrap", null, errorContent);
            section[0].appendChild(wrapDiv)

        })


}

function getLimit(element) {
    if (event.key === 'Enter') {
        getAllProducts(`https://fakestoreapi.com/products?limit=${element.value}`)

    }

}

// Select by category.

function createDropDown() {
    fetchData('https://fakestoreapi.com/products/categories')
        .then((data) => {
            var div = document.getElementById("dropdown"),
                frag = document.createDocumentFragment(),
                select = document.createElement("select");
            select.setAttribute("onChange", "selectedCategory()")
            select.classList.add("dropdown")
            // console.log(div);
            data.forEach((category, index) => {
                if (index === 0) {
                    select.options.add(new Option("All", true));
                }
                select.options.add(new Option(category));
            });


            frag.appendChild(select);
            div.appendChild(frag);


        })

}

createDropDown()

function selectedCategory(selectedValue) {
    if(!selectedValue){
        selectedValue = document.getElementsByClassName("dropdown")[0].value;
    }  
    let section = document.getElementsByClassName("section-one-part-two");
    document.getElementsByClassName("dropdown")[0].value = selectedValue;
    console.log(selectedValue);
    section[0].innerHTML = ""
    if (selectedValue === "true") {
        getAllProducts("https://fakestoreapi.com/products");
    } else {
        getAllProducts(`https://fakestoreapi.com/products/category/${selectedValue}`)
    }
    

}

// Sort
function sort(element){
    if (element.name === "asc"){
        getAllProducts('https://fakestoreapi.com/products?sort=desc')
        element.name = "desc";
    } else {
        getAllProducts('https://fakestoreapi.com/products?sort=asc')
        element.name = "asc";
    }
}

function getCategories(){
    let section = document.getElementsByClassName("section-one-part-two");
    section[0].innerHTML = ""
    fetchData('https://fakestoreapi.com/products/categories')
    .then((data)=>{
        data.forEach((category)=>{
            let titleContent = `<div class="title">
                                        <h1><a onclick="setCategory(this)">${category}</a></h1>
                                    </div>`
            let wrapDiv = createAElement("div", "wrap", "wrap", titleContent);


            section[0].appendChild(wrapDiv)
        })
    })
    
        
}
function setCategory(element) {
    selectedCategory(element.textContent);
    console.log(element.textContent);
}


function setupSkeletonTemplate(){
    let grid = document.querySelector('.contents')

    let cardTemplate = document.getElementsByClassName('undefined')[0]

    for (let i = 0; i < 5; i++) {
        grid.append(cardTemplate.cloneNode(true))
    }
}
setupSkeletonTemplate();

const GET_ALL_PRODUCTS_API = "https://fakestoreapi.com/products";

getAllProducts(GET_ALL_PRODUCTS_API);


function logout() {
    sessionStorage.removeItem("isLoggedIn");
    alert("Logged Out successfully.");
    window.location.reload();
}

function loggedIn() {
    let login = document.getElementsByClassName("login")[0]
    login.style.display = "none"
    let signup = document.getElementsByClassName("signup")[0]
    signup.style.display = "none"
    let card = document.getElementsByClassName("card");
    card[0].innerHTML = "";
    let element = document.createElement("p");
    element.innerHTML = "You are currently logged in. Click logout to perform other operations."
    card[0].appendChild(element)

}

if (sessionStorage.getItem("isLoggedIn")) {
    loggedIn()
} else {
    let logout = document.getElementsByClassName("logout")[0]
    logout.style.display = "none"
}