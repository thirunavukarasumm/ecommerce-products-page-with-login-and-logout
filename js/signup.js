function containsSpecialChars(str) {
    const specialChars =
        '[`!@#$%^&*()_+-=[]{};\':"\\|,.<>/?~]/';
    return specialChars
        .split('')
        .some((specialChar) => str.includes(specialChar));
}

function passwordValidator(password) {
    let validRegex = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    return validRegex.test(password)
}
function inputValidation(event) {
    event.preventDefault();
    for (let element in event.target) {
        if (element === "5") {
            break
        }
        if (!event.target[element].value) {
            let label = document.getElementById(event.target[element].name)
            label.innerHTML = "This field should not be empty."
            event.target[element].style.border = "2px solid red"
            break;
        } else {
            
            if (Number(element) < 2 && containsSpecialChars(event.target[element].value)) {
                let label = document.getElementById(event.target[element].name);
                label.innerHTML = "This field consists of special characters."
                break
            } else {
                let label = document.getElementById(event.target[element].name)
                label.innerHTML = ""
                event.target[element].style.border = "none"
            }
            
        }
        if(element == 2){
            let label = document.getElementById(event.target[element].name);
            if (localStorage.getItem(event.target[element].value)){
                label.innerHTML = "A user already exists in the given email.";
                break;
            } else {
                label.innerHTML = "";

            }
        }
        if(element == 3){
            let label = document.getElementById(event.target[element].name);
            if (!passwordValidator(event.target[element].value)){
                label.innerHTML = "Password doesn't meet requirements. Example - Test@123";
                event.target[element].style.display = "2px solid red";
                break;
            } else {
                label.innerHTML = "";
                event.target[element].style.display = "none";
            }
        }
        if(element == 4){
            let label = document.getElementById(event.target[element].name)
            console.log(event.target[3].value, event.target[4].value);
            if (event.target[3].value !== event.target[element].value) {
                label.innerHTML = "Passwords doesn't match.";
                event.target[element].style.border = "2px solid red";
                break;
            } else {
                console.log("matching");
                label.innerHTML = ""
                event.target[element].style.border = "none"
            }
            if (!document.getElementById("terms").checked) {
                alert("Accept terms and conditions.")
            } else {
                let user = {};
                user["firstname"] = event["target"][0].value
                user["lastname"] = event["target"][1].value
                user["password"] = event["target"][3].value
                localStorage.setItem(event["target"][2].value,JSON.stringify(user));
                sessionStorage.setItem("isLoggedIn",true);
                alert("SignUp successful.")
                window.location = "/"
            }
        }
    }
    
    
    
}

function logout(){
    sessionStorage.removeItem("isLoggedIn");
    alert("Logged Out successfully.");
    window.location.reload();
}

function loggedIn(){
        let login = document.getElementsByClassName("login")[0]
        login.style.display = "none"
        let card = document.getElementsByClassName("card");
        card[0].innerHTML = "";
        let element = document.createElement("p");
        element.innerHTML = "You are currently logged in. Click logout to perform other operations."
        card[0].appendChild(element)

}

if (sessionStorage.getItem("isLoggedIn")) {
    loggedIn()
} else {
    let logout = document.getElementsByClassName("logout")[0]
    logout.style.display = "none"
}