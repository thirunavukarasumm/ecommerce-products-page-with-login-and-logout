function signIn(event){
    event.preventDefault()
    let userName = document.getElementById("email");
    let password = document.getElementById("password");
    let emailField = document.getElementById("email-field");
    let passwordField = document.getElementById("password-one");
    let user = JSON.parse(localStorage.getItem(userName.value));
    if(!user){
        emailField.innerHTML = "User does not exists in the provided email.";
        userName.style.border = "2px solid red";
        return;
    } else {
        emailField.innerHTML = "";
        userName.style.border = "none";
        if(password.value){
            passwordField.innerHTML = "";
            password.style.border = "none";
            if (password.value != user["password"]) { 
                passwordField.innerHTML = `Password does not match, Your password is "${user["password"]}"`;
                password.style.border = "2px solid red";
                return;
            } else {
                passwordField.innerHTML = ``;
                password.style.border = "none";
                sessionStorage.setItem("isLoggedIn", true);
                alert("Logged in.")
                window.location = "/";
            }
        } else {
            passwordField.innerHTML = `Password should not be empty, Your password is "${user["password"]}"`;
            password.style.border = "2px solid red";
        }
    }
}


function logout() {
    sessionStorage.removeItem("isLoggedIn");
    alert("Logged Out successfully.");
    window.location.reload();
}

function loggedIn() {
    let signup = document.getElementsByClassName("signup")[0]
    signup.style.display = "none"
    let card = document.getElementsByClassName("card");
    card[0].innerHTML = "";
    let element = document.createElement("p");
    element.innerHTML = "You are currently logged in. Click logout to perform other operations."
    card[0].appendChild(element)
}

if (sessionStorage.getItem("isLoggedIn")) {
    loggedIn()
} else {
    let logout = document.getElementsByClassName("logout")[0]
    logout.style.display = "none"
}